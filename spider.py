# coding: utf-8

import ipdb
import sys
from selenium import webdriver
from scrapy import Selector
from time import sleep
from termcolor import colored
from utils import colors

"""
D - dog macho
B - bitch fêmea
Bends - posição em cada curva
Grade - categoria que correu
Fin - posição final
Corrida de distancia longa FEMEA tem vantagem
Corrida de distancia curta MACHO tem vantagem
Analisar categorias
Analisar os cachorros que vem subindo de produção e os que vem caindo de produção
"""

class SpiderGalgos(object):

    def __init__(self):
        self.driver = webdriver.PhantomJS('/usr/bin/phantomjs')

    def parser(self, url):
        self.driver.get(url)
        sleep(7)
        page_source = self.driver.page_source.encode('utf-8')

        cards_html = Selector(text=page_source).css("#card-scroll div.runnerBlock").extract()
        title_run = self.extract_content(page_source,
                                         "//*[@id='title-circle-container']//span[@class='titleColumn2']/text()")
        distance_run = title_run.split(" ")[2].strip()
        diff_distances = []
        print '\n==================================== {} ================================\n'.format(title_run)
        try:
            for i, card in enumerate(cards_html):
                splits = []
                cal_tms = []
                distances = []
                dates = []
                grades = []
                fins = []

                for count in range(2, 8):
                    genre_selector = "//table[@class='dogInfo']//tr[@class='dogDetails']/td/text()"
                    dog_name_selector = "//a[contains(@class, 'dogName')]/strong/text()"
                    date_selector = "//table[@class='formGrid desktop ']/tbody/tr[{}]/td[contains(@class,'c0 ')]/a/text() | //table[@class='formGrid desktop ']/tbody/tr[{}]/td[contains(@class,'c0 ')]/text()".format(
                        count, count)
                    distance_selector = "//table[@class='formGrid desktop ']/tbody/tr[{}]/td[3]/text()".format(count)
                    split_selector = "//table[@class='formGrid desktop ']/tbody/tr[{}]/td[5]/text()".format(count)
                    cal_tm_selector = "//table[@class='formGrid desktop ']/tbody/tr[{}]/td[16]/text()".format(count)
                    bends_selector = "//table[@class='formGrid desktop ']/tbody/tr[{}]/td[@class='c4']/text()".format(
                        count)
                    grade_selector = "//table[@class='formGrid desktop ']/tbody/tr[{}]/td[15]/text()".format(count)
                    fin_selector = "//table[@class='formGrid desktop ']/tbody/tr[{}]/td[7]/text()".format(count)

                    distance = self.extract_content(card, distance_selector)
                    if distance == distance_run:
                        distances.append(distance)

                        dog_name = self.extract_content(card, dog_name_selector)
                        dates.append(self.extract_content(card, date_selector))
                        splits.append(self.extract_content(card, split_selector))
                        cal_tms.append(self.extract_content(card, cal_tm_selector))
                        genre = self.define_genre(self.extract_content(card, genre_selector))
                        bends = [b for b in self.extract_content(card, bends_selector)]
                        grades.append(self.extract_content(card, grade_selector))
                        fins.append(self.extract_content(card, fin_selector))


                print "Galgo: {} - Name: {} - Genre: {}".format(i + 1, dog_name, genre)
                print "Date: {}".format(dates)
                print "Distances: {}".format(distances)
                print "Splits: {}".format(splits)
                print "Bends: {}".format(bends)
                print "Fins: {}".format(fins)
                print "Grades: {}".format(grades)
                print "CalTms: {}".format(cal_tms)
                print colored("--------------------------------------------------------------------------", "yellow")
        except Exception, e:
            print e
        finally:
            self.driver.quit()

    def extract_content(self, html, selector, selector_type="xpath"):
        method = getattr(Selector(text=html), selector_type)
        value = method(selector).extract()
        if len(value) > 1:
            for val in value:
                val = val.strip()
                if val:
                    value = [val]
                    break
        value = value[0].strip() if value else ""
        return value

    def define_genre(self, value):
        list_values = value.split(" ")
        value = list_values[1] if len(list_values) >= 2 else "n"
        genres = {"d": "Macho", "b": "Femea", "n": "Sem genero"}
        return genres.get(value)

	def show_stats(self):
		pass


cmdargs = sys.argv

if len(cmdargs) > 1:
	url = cmdargs[1]
	print '\n\n', url, '\n\n'
	spider = SpiderGalgos()
	spider.parser(url)
else:
	print colored("\nmiss parameter: [URL required]\n", "red")
