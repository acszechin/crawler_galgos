# coding: utf-8

def colors():
    return {
        1: {'fg': 'white', 'bg': 'on_red'},
        2: {'fg': 'white', 'bg': 'on_blue'},
        3: {'fg': 'black', 'bg': 'on_white'},
        4: {'fg': 'white', 'bg': 'on_red'},
        5: {'fg': 'white', 'bg': 'on_orange'},
        6: {'fg': 'red', 'bg': 'on_gray'},
    }